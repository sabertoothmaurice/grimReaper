package com.maurice.grimreaper.grimreaper.Activities.Main;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.maurice.grimreaper.grimreaper.Models.CardObj;
import com.maurice.grimreaper.grimreaper.R;
import com.maurice.grimreaper.grimreaper.Views.BankCardViewBuilder;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.util.ArrayList;

/**
 * Created by maurice on 10/06/15.
 */
public class CardsFragAdapter extends ArrayAdapter<CardObj> {
    public static final String TAG = "CardsFragAdapter";
    Activity mContext;
    private final ArrayList<CardObj> cards;
    DisplayImageOptions options_map;

    @Override
    public int getCount() {
        return cards.size();
    }

    public CardsFragAdapter(Activity context, ArrayList<CardObj> cards){
        super(context, R.layout.mycards_list_item, cards);
        this.mContext = context;
        this.cards = cards;

        options_map = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.login_wallpaper)
                .showImageForEmptyUri(R.drawable.login_wallpaper)
                .showImageOnFail(R.drawable.login_wallpaper)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = null;
        if (convertView == null) {
            view = BankCardViewBuilder.getBankCardView(mContext);
        } else {
            view = convertView;
        }

        final CardObj msg = cards.get(position);
        BankCardViewBuilder.ViewHolder holder = (BankCardViewBuilder.ViewHolder) view.getTag();
        holder.inflateData(msg);

        return view;
    }


}
