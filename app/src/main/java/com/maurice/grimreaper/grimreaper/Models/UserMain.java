package com.maurice.grimreaper.grimreaper.Models;

import android.content.Context;

import com.maurice.grimreaper.grimreaper.storage.SharedPrefs;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This contains all the User data excluding kids,
 */
public class UserMain {
    private Context mContext;
    private static UserMain instance;
    private SharedPrefs sPrefs;
    public String name;
    public String userId;
    public String xmppPass;
    public String email;
    public String imageUrl;
    public String gcmId;
    public String facebookId;
    public String phone;
    public String address;
    public String coverPic;
    public Boolean isDoctor = false;
    public String userType; //can takes values "parent","doctor","none"
    public Boolean isMale = false;
    public String token;
    public String authProvider;


    private UserMain(Context context) {
        mContext = context;
        pullUserDataFromLocal();
    }
    public static UserMain getInstance(Context context) {
        if(instance == null) {
            instance = new UserMain(context);
        }
        return instance;
    }


    //LOCAL STORAGE ENCODERS
    public void pullUserDataFromLocal() {
        sPrefs = SharedPrefs.getInstance(mContext);
        try {
            name = (sPrefs.userData.has("name"))?sPrefs.userData.getString("name"):"";
            phone = (sPrefs.userData.has("phone"))?sPrefs.userData.getString("phone"):"";
            userId = (sPrefs.userData.has("userId"))?sPrefs.userData.getString("userId"):"";
        } catch (JSONException e) {e.printStackTrace();}
    }
    public void saveUserDataLocally() {
        try {
            sPrefs.userData.put("userId", userId);
            sPrefs.userData.put("phone", phone);
            sPrefs.userData.put("name", name);
        } catch (JSONException e) {e.printStackTrace();}
        sPrefs.saveUserData();
    }

    public void saveUserImagesLocally() {
        try {
            sPrefs.userData.put("imageUrl", imageUrl);
            sPrefs.userData.put("coverPic", coverPic);
        } catch (JSONException e) {e.printStackTrace();}
        sPrefs.saveUserData();
    }

    //SERVER ENCODERS
    public void decodeFromServer(JSONObject obj2){
        try {
            if(obj2.has("user")){
                JSONObject obj = obj2.getJSONObject("user");
                name = (obj.has("username"))?obj.getString("username"):"";
                userId = (obj.has("userId"))?obj.getString("userId"):"";
                phone = (obj.has("mobileNumber"))?obj.getString("mobileNumber"):"";
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public JSONObject encodeForServer(){
        JSONObject jsonObject = new JSONObject();
        try {
            if (facebookId != null && !facebookId.isEmpty()) jsonObject.put("facebookId",facebookId);
            if (imageUrl != null && !imageUrl.isEmpty()) jsonObject.put("profilepic",imageUrl);
            if (xmppPass != null && !xmppPass.isEmpty()) jsonObject.put("xmppPass", xmppPass);//todo:check if we need to send this
            if (userType != null && !userType.isEmpty()) jsonObject.put("userType",userType);
            if (coverPic != null && !coverPic.isEmpty()) jsonObject.put("coverpic",coverPic);
            if (address != null && !address.isEmpty()) jsonObject.put("address",address);
            if (userId != null && !userId.isEmpty()) jsonObject.put("userId",userId);
            if (phone != null && !phone.isEmpty()) jsonObject.put("phone",phone);
            if (email != null && !email.isEmpty()) jsonObject.put("email",email);
            if (gcmId != null && !gcmId.isEmpty()) jsonObject.put("gcmId", gcmId);
            if (name != null && !name.isEmpty()) jsonObject.put("name",name);
            if (isDoctor != null) jsonObject.put("isDoctor", isDoctor);
            if (isMale != null) jsonObject.put("isMale", isMale);
            if (token != null && !token.isEmpty()) jsonObject.put("token", token);
            if (authProvider != null && !authProvider.isEmpty()) jsonObject.put("authProvider", authProvider);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    /** This pushes and updates user object in server */
//    public void serverSyncPush(){
//        String url = Router.User.addMarker(MainApplication.getInstance());
//        JSONObject jsonObject = encodeForServer();
//        Log.e("MAIN","Sending  serverSyncPush");
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//                Logg.m("MAIN", "Successfully synced user with server : " + response.toString());
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d("ERROR", "Failed to sync user with server : "+error.getLocalizedMessage());
//            }
//        });
//        MainApplication.getInstance().getRequestQueue().add(jsonObjectRequest);
//    }
}
