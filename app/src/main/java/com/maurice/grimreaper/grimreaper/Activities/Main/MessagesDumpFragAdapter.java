package com.maurice.grimreaper.grimreaper.Activities.Main;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.maurice.grimreaper.grimreaper.Models.SMSDirectory;
import com.maurice.grimreaper.grimreaper.Models.SMSMessage;
import com.maurice.grimreaper.grimreaper.R;

import java.util.ArrayList;

/**
 * Created by maurice on 10/06/15.
 */
public class MessagesDumpFragAdapter extends ArrayAdapter<SMSDirectory> {
    Activity mContext;
    private final ArrayList<SMSDirectory> SMSDirectories;

    @Override
    public int getCount() {
        return SMSDirectories.size();
    }

    public MessagesDumpFragAdapter(Activity context, ArrayList<SMSDirectory> SMSDirectories){
        super(context, R.layout.smslist_list_item, SMSDirectories);
        this.mContext = context;
        this.SMSDirectories = SMSDirectories;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = null;
        if (convertView == null) {
            LayoutInflater inflator = mContext.getLayoutInflater();
            view = inflator.inflate( R.layout.smslist_list_item, null );

            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.tv_header = (TextView) view.findViewById(R.id.tv_header);
            viewHolder.tv_body = (TextView) view.findViewById(R.id.tv_body);
            viewHolder.tv_body2 = (TextView) view.findViewById(R.id.tv_body2);

            view.setTag(viewHolder);
        } else {
            view = convertView;
        }

        SMSDirectory directory = SMSDirectories.get(position);
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.tv_header.setText(directory.from+"("+directory.messages.size()+")");
        SMSMessage firstMsg = directory.messages.get(0);
        holder.tv_body.setText((firstMsg.body.length()>45)?directory.messages.get(0).body.substring(0,40)+"....":firstMsg.body);
        holder.tv_body2.setText(directory.messages.get(0).getTimeString());

        final String from = directory.from;
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,SMSDirectoryActivity.class);
                intent.putExtra(SMSDirectoryActivity.INTENT_SMS_FROM,from);
                mContext.startActivityForResult(intent,200);
            }
        });

        return view;
    }

    static class ViewHolder {
        public TextView tv_header, tv_body, tv_body2;
    }

}
