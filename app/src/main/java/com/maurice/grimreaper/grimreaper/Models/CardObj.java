package com.maurice.grimreaper.grimreaper.Models;

import android.text.Spanned;

import com.maurice.grimreaper.grimreaper.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class CardObj implements Comparable<CardObj> {

    public List<Balance> balances = new ArrayList<>();
    public List<Transaction> transactions = new ArrayList<>();
    public List<String> variables = new ArrayList<>();
    public String vendorId;
    public Long time;
    public String _id,category,regex;
    public String cardName,vendorName,logoUrl;
    public HashMap<String,Object> extras = new HashMap<>();

    public CardObj() {}

    //SERVER ENCODERS
    public static CardObj decode(JSONObject obj){

        CardObj re = new CardObj();
        try {
            JSONObject cardData = (obj.has("cardData")) ? obj.getJSONObject("cardData") : new JSONObject();
            JSONArray balanceArr = (cardData.has("balances")) ? cardData.getJSONArray("balances") : new JSONArray();
            re.balances.addAll(Balance.decode(balanceArr));
            JSONArray transactionsArr = (cardData.has("costs")) ? cardData.getJSONArray("costs") : new JSONArray();
            re.transactions.addAll(Transaction.decode(transactionsArr));
            re.cardName = (obj.has("cardName")) ? obj.getString("cardName") : null;
            re.vendorName = (obj.has("vendorName")) ? obj.getString("vendorName") : null;
            re.logoUrl = (obj.has("logo")) ? obj.getString("logo") : null;

        } catch (JSONException e) {e.printStackTrace();}
        return re;
    }

    public static ArrayList<CardObj> decode(JSONArray obj){

        ArrayList<CardObj> list = new ArrayList<>();
        for(int i=0;i<obj.length();i++){
            try {
                list.add(decode(obj.getJSONObject(i)));
            } catch (JSONException e) {e.printStackTrace();}
        }
        return list;
    }

    public JSONObject encode(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put( "vendorId", vendorId );
            jsonObject.put( "time", time );
            jsonObject.put( "_id", _id );

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public int compareTo(CardObj another) {
        return 0;//TODO : implement this
    }

    public Spanned getHighlightedBody(){
        String body = ""+regex;
        body = body.replace("\\","");
        ArrayList<String> highlightTextArr = new ArrayList<>();
        for(String varName : variables){
            int index = body.indexOf("(.+)");
            body = body.substring(0, index) + "["+varName+"]" + body.substring(index+4);
            highlightTextArr.add("["+varName+"]");
        }
        return Utils.highlightText(highlightTextArr, body);
    }

    public static class Balance{
        public long time;
        public double balance;
        Balance(){}
        public static Balance decode(JSONObject obj){
            Balance re = new Balance();
            try {
                re.time = (obj.has("time")) ? obj.getLong("time") : Calendar.getInstance().getTimeInMillis();
                re.balance = (obj.has("balance")) ? obj.getLong("balance") : 0L;
            } catch (JSONException e) {e.printStackTrace();}
            return re;
        }

        public static ArrayList<Balance> decode(JSONArray obj){
            ArrayList<Balance> list = new ArrayList<>();
            for(int i=0;i<obj.length();i++){
                try {
                    list.add(decode(obj.getJSONObject(i)));
                } catch (JSONException e) {e.printStackTrace();}
            }
            return list;
        }
        public int getDaysAgo(){
            long millis = Calendar.getInstance().getTimeInMillis()-time;
            return (int)(millis/(1000*60*60*24));
        }
    }
    public static class Transaction{
        public long time;
        public double cost;
        Transaction(){}
        public static Transaction decode(JSONObject obj){
            Transaction re = new Transaction();
            try {
                re.time = (obj.has("time")) ? obj.getLong("time") : Calendar.getInstance().getTimeInMillis();
                re.cost = (obj.has("cost")) ? obj.getLong("cost") : 0L;
            } catch (JSONException e) {e.printStackTrace();}
            return re;
        }

        public static ArrayList<Transaction> decode(JSONArray obj){
            ArrayList<Transaction> list = new ArrayList<>();
            for(int i=0;i<obj.length();i++){
                try {
                    list.add(decode(obj.getJSONObject(i)));
                } catch (JSONException e) {e.printStackTrace();}
            }
            return list;
        }
        public int getDaysAgo(){
            long millis = Calendar.getInstance().getTimeInMillis()-time;
            return (int)(millis/(1000*60*60*24));
        }
    }
}
