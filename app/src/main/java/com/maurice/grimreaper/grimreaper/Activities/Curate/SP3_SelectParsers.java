package com.maurice.grimreaper.grimreaper.Activities.Curate;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.maurice.grimreaper.grimreaper.Controllers.ToastMain;
import com.maurice.grimreaper.grimreaper.MainApplication;
import com.maurice.grimreaper.grimreaper.Models.CurateSMSParser;
import com.maurice.grimreaper.grimreaper.Models.VendorObj;
import com.maurice.grimreaper.grimreaper.R;
import com.maurice.grimreaper.grimreaper.Utils.Logg;
import com.maurice.grimreaper.grimreaper.Utils.Router;
import com.maurice.grimreaper.grimreaper.Utils.Utils;
import com.wefika.flowlayout.FlowLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SP3_SelectParsers extends AppCompatActivity {
    public static final String TAG = "SP3";
    public static final String MESSAGE = "MESSAGE";

    CurateSMSParser curateSMSParser;
    FlowLayout flowCont;
    SeekBar sb_start, sb_end;
    TextView tv_seek;
    LayoutInflater inflator;
    ArrayList<String> bodyAsArrStr = new ArrayList<>();
    Integer selectedIndex = -1;
    String selectedString;
    Spinner sp_vendors;
    ArrayList<VendorObj> vendors = new ArrayList<>();
    VendorObj vendorSelected;
    int prefix_start,prefix_end,suffix_start,suffix_end;
    int startIndex,  endIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sp3__select_parsers);
        curateSMSParser = (CurateSMSParser) getIntent().getSerializableExtra(MESSAGE);
        inflator = getLayoutInflater();
        bodyAsArrStr = curateSMSParser.sampleMsg.returnBodyAsArrInt();

        flowCont = (FlowLayout) findViewById(R.id.flowCont);
        sb_start = (SeekBar) findViewById(R.id.sb_start);
        sb_end = (SeekBar) findViewById(R.id.sb_end);
        tv_seek = (TextView) findViewById(R.id.tv_seek);
        sp_vendors = (Spinner)findViewById(R.id.sp_vendors);

        findViewById(R.id.btn_test).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showMatchedMessagesInAlert();
            }
        });
        findViewById(R.id.btn_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitCompleteDataToServer();
            }
        });
        generatePossibleNumMatches();
        fetchVendorsData();
    }


    private void generatePossibleNumMatches(){
        int index = 0;
        flowCont.removeAllViews();
        for(String str : bodyAsArrStr){
            View cont = inflator.inflate(R.layout.sp2_button, flowCont, false);
            TextView textView = (TextView) cont.findViewById(R.id.tv_main);
            if(selectedHasIndex(index)){
                textView.setBackgroundColor(Color.GREEN);
            }else{
                textView.setBackgroundColor(Color.RED);
            }


            textView.setText(str);
            final int strFinalIndex = 0+index;
            cont.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleString(strFinalIndex);
                    generatePossibleNumMatches();
                    generatePossiblePreSuffixMatches();
                }
            });
            flowCont.addView(cont);
            index++;
        }
    }

    private void generatePossiblePreSuffixMatches(){
        if(selectedString==null) return;


        startIndex = curateSMSParser.sampleMsg.body.indexOf(selectedString);
        endIndex = startIndex+selectedString.length();
        int totalLength = curateSMSParser.sampleMsg.body.length();

        sb_start.setMax(startIndex);
        sb_end.setMax(totalLength-endIndex);
        prefix_start = startIndex;
        prefix_end = startIndex;
        suffix_start = endIndex;
        suffix_end = endIndex;
        sb_start.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                prefix_start = progress;
                refreshSeekText();
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
        sb_end.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                suffix_end = endIndex+progress;
                refreshSeekText();
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
        refreshSeekText();
    }
    private void refreshSeekText(){
//        Logg.d("Refreshing seek : "," : "+prefix_start+" : "+prefix_end+" :"+suffix_start+" :"+suffix_end);
        int showStart = prefix_start-6;if(showStart<0)showStart=0;
        int showEnd = suffix_end+6;if(showEnd>curateSMSParser.sampleMsg.body.length())showEnd=curateSMSParser.sampleMsg.body.length();
        Spanned prefix = Utils.highlightTextIndex(curateSMSParser.sampleMsg.body.substring(showStart,showEnd),prefix_start-showStart,suffix_end-showStart);
        tv_seek.setText(prefix);
    }



    private void toggleString(int toggledIndex) {
        selectedIndex = toggledIndex;
        selectedString = bodyAsArrStr.get(toggledIndex);

    }

    public boolean selectedHasIndex(int index){
        return (index==selectedIndex);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sp2__select_scanners, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showMatchedMessagesInAlert(){
        final Dialog dialog1 = new Dialog(this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.sp2_alertbox_test);
        ListView listView = (ListView) dialog1.findViewById(R.id.listView);
        final ArrayAdapter<Spanned> arrayAdapter = new ArrayAdapter<>(this,
                R.layout.sp2_alertbox_item);

        dialog1.findViewById(R.id.btn_yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });


        listView.setAdapter(arrayAdapter);


        dialog1.show();
    }
    private void fetchVendorsData(){
        String url = Router.Curate.allVendors();
        MainApplication.getInstance().addRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Logg.d(TAG, "Vendors DATA : " + jsonObject.toString());
                try {
                    JSONObject result = jsonObject.getJSONObject("result");
                    JSONArray vendorsJSON = result.has("vendors") ? result.getJSONArray("vendors") : new JSONArray();
                    vendors.clear();
                    vendors.addAll(VendorObj.decode(vendorsJSON));

                    ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(SP3_SelectParsers.this, android.R.layout.simple_spinner_item, android.R.id.text1);
                    spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    sp_vendors.setAdapter(spinnerAdapter);
                    for (VendorObj vendor : vendors) {
                        spinnerAdapter.add(vendor.vendorName);
                    }
                    sp_vendors.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            vendorSelected = vendors.get(position);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {}
                    });
                    spinnerAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Logg.e(TAG, "ERROR : " + volleyError);
            }
        });
    }
    private void submitCompleteDataToServer(){
        String url = Router.Curate.createRegex();
        JSONObject params = new JSONObject();
        try {
            params.put("vendorId",vendorSelected.vendorId);
            params.put("sample",curateSMSParser.sampleMsg.body);
            params.put("from",curateSMSParser.sampleMsg.number);

            JSONArray arr = new JSONArray();
            for(String match : curateSMSParser.matchers) arr.put(match);
            params.put("match",arr);
            params.put("prefix",curateSMSParser.sampleMsg.body.substring(prefix_start,prefix_end));
            params.put("suffix",curateSMSParser.sampleMsg.body.substring(suffix_start,suffix_end));
        } catch (JSONException e) {e.printStackTrace();}
        MainApplication.getInstance().addRequest(Request.Method.POST, url,params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Logg.d(TAG, "Post REGEX COMPLETE : " + jsonObject.toString());
                try {
                    JSONObject result = jsonObject.getJSONObject("result");
                    ToastMain.showSmartToast(SP3_SelectParsers.this,"Successfully posted to server");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Logg.e(TAG, "ERROR : " + volleyError);
            }
        });
    }
}
