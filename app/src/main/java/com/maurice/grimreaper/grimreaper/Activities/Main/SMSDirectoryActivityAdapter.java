package com.maurice.grimreaper.grimreaper.Activities.Main;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.maurice.grimreaper.grimreaper.Models.SMSMessage;
import com.maurice.grimreaper.grimreaper.R;

import java.util.ArrayList;

/**
 * Created by maurice on 10/06/15.
 */
public class SMSDirectoryActivityAdapter extends ArrayAdapter<SMSMessage> {
    Activity mContext;
    private final ArrayList<SMSMessage> smsMessages;

    @Override
    public int getCount() {
        return smsMessages.size();
    }

    public SMSDirectoryActivityAdapter(Activity context, ArrayList<SMSMessage> smsMessages){
        super(context, R.layout.smslist_list_item, smsMessages);
        this.mContext = context;
        this.smsMessages = smsMessages;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = null;
        if (convertView == null) {
            LayoutInflater inflator = mContext.getLayoutInflater();
            view = inflator.inflate( R.layout.smslist_list_item, null );

            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.tv_header = (TextView) view.findViewById(R.id.tv_header);
            viewHolder.tv_body = (TextView) view.findViewById(R.id.tv_body);
            viewHolder.tv_body2 = (TextView) view.findViewById(R.id.tv_body2);

            view.setTag(viewHolder);
        } else {
            view = convertView;
        }

        SMSMessage message = smsMessages.get(position);
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.tv_header.setText(message.number);
        holder.tv_body.setText(message.body);
        holder.tv_body2.setText(message.getTimeString());

        return view;
    }

    static class ViewHolder {
        public TextView tv_header, tv_body, tv_body2;
    }

}
