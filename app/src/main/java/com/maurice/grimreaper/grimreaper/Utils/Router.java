package com.maurice.grimreaper.grimreaper.Utils;

import android.net.Uri;

import com.maurice.grimreaper.grimreaper.MainApplication;

/**
 * This contains all the routes to our backend server. This is similar to API CONTRACT given to backend team.
 * Any changes with backend API routes will only be reflected by changes in this FIle.
 */
public class Router {

    private static final String DEFAULT_SCHEME = "http";
    private static final String DEFAULT_AUTHORITY = Settings.BASE_AUTHORITY;//"api.maurice.com"
    private static final String API_VERSION = "2.3.2";

    public static String getServerToken() {
        return MainApplication.getInstance().tokenHandler.servertoken;
    };

    private static Uri.Builder getNewDefaultBuilder() {
        return new Uri.Builder().scheme(DEFAULT_SCHEME).encodedAuthority(DEFAULT_AUTHORITY).appendQueryParameter("version",API_VERSION);
    }


    public static class User{
//        private static String base = Settings.BASE_URL+"mobile/v1";

//        public static String base(){return base; }
//        public static String signUp(String mobile, String pass){return base+"/signup?mobile="+mobile+"&password="+pass; }
//        public static String login(String mobile, String pass){return base+"/login?mobile="+mobile+"&password="+pass; }
//        public static String addMarker(String userId){return base+"/"+userId+"/addMarker"; }

        public static String googlelogin(String googleToken, String email){
//            return base+"/login/google?access_token="+access_token;
            //http://api.clozerr.com/v2/user/details/get?access_token=8738307ee2b3851ae25025b830de2f73
            return getNewDefaultBuilder().path("login/google")
                    .appendQueryParameter("email", email)//Should be removed once Google Auth is fixed
                    .appendQueryParameter("google_token", googleToken).build().toString();
        }
    }

    public static class SMS{

        public static String dump(){
            return getNewDefaultBuilder().path("sms/dump")
                    .appendQueryParameter("access_token", getServerToken()).build().toString();
        }
    }
    public static class Data{

        public static String transactions(){
            return getNewDefaultBuilder().path("data/transactions")
                    .appendQueryParameter("access_token", getServerToken()).build().toString();
        }

        public static String mycards(){
            return getNewDefaultBuilder().path("data/mycards")
                    .appendQueryParameter("access_token", getServerToken()).build().toString();
        }

        public static String regexs(){
            return getNewDefaultBuilder().path("data/allregexs")
                    .appendQueryParameter("access_token", getServerToken()).build().toString();
        }
    }
    public static class Curate{

        public static String allVendors(){
            return getNewDefaultBuilder().path("data/allvendors")
                    .appendQueryParameter("access_token", getServerToken()).build().toString();
        }
        public static String editBucket(){
            return getNewDefaultBuilder().path("data/editbucket")
                    .appendQueryParameter("access_token", getServerToken()).build().toString();
        }
        public static String createRegex(){
            return getNewDefaultBuilder().path("data/createregex")
                    .appendQueryParameter("access_token", getServerToken()).build().toString();
        }
        public static String deleteRegex(String regexId){
            return getNewDefaultBuilder().path("data/deleteregex")
                    .appendQueryParameter("regexId", regexId)
                    .appendQueryParameter("access_token", getServerToken()).build().toString();
        }

    }
}


