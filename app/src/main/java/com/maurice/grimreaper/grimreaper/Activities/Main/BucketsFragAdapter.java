package com.maurice.grimreaper.grimreaper.Activities.Main;

import android.app.Activity;
import android.app.Dialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.maurice.grimreaper.grimreaper.Controllers.LocalBroadcastHandler;
import com.maurice.grimreaper.grimreaper.MainApplication;
import com.maurice.grimreaper.grimreaper.Models.BucketObj;
import com.maurice.grimreaper.grimreaper.Models.VendorObj;
import com.maurice.grimreaper.grimreaper.R;
import com.maurice.grimreaper.grimreaper.Utils.Logg;
import com.maurice.grimreaper.grimreaper.Utils.Router;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by maurice on 10/06/15.
 */
public class BucketsFragAdapter extends ArrayAdapter<BucketObj> {
    public static final String TAG = "RegexDefFragAdapter";
    Activity mContext;
    private final ArrayList<BucketObj> transactions;

    @Override
    public int getCount() {
        return transactions.size();
    }

    public BucketsFragAdapter(Activity context, ArrayList<BucketObj> transactions){
        super(context, R.layout.transactions_list_item, transactions);
        this.mContext = context;
        this.transactions = transactions;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = null;
        if (convertView == null) {
            LayoutInflater inflator = mContext.getLayoutInflater();
            view = inflator.inflate( R.layout.regexs_list_item, null );

            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.tv_header = (TextView) view.findViewById(R.id.tv_header);
            viewHolder.tv_subheader = (TextView) view.findViewById(R.id.tv_subheader);
            viewHolder.tv_from = (TextView) view.findViewById(R.id.tv_from);
            viewHolder.tv_body = (TextView) view.findViewById(R.id.tv_body);

            view.setTag(viewHolder);
        } else {
            view = convertView;
        }

        final BucketObj msg = transactions.get(position);
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.tv_header.setText(msg.name+"");
        holder.tv_subheader.setText("("+msg.matches+")");
        holder.tv_from.setText(msg.from);
        holder.tv_body.setText(msg.getHighlightedBody());


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = Router.Curate.editBucket();
                MainApplication.getInstance().addRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Logg.d(TAG, "Get edit data : " + jsonObject.toString());
                        try {
                            JSONObject result = jsonObject.getJSONObject("result");
                            JSONArray categories = (result.has("categories")) ? result.getJSONArray("categories") : new JSONArray();
                            JSONArray vendorsJSON = result.has("vendors") ? result.getJSONArray("vendors") : new JSONArray();
                            ArrayList<VendorObj> vendors = VendorObj.decode(vendorsJSON);
                            showConfirmRemoveDialog(msg,categories,vendors);
                        } catch (JSONException e) {e.printStackTrace();}
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Logg.e(TAG, "ERROR : " + volleyError);
                    }
                });
            }
        });
        return view;
    }


    private void showConfirmRemoveDialog(final BucketObj msg, final JSONArray categories, final ArrayList<VendorObj> vendors){
        final Dialog dialog1 = new Dialog(mContext);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.bucket_editor_alertbox);
        final EditText et_bucketname = (EditText) dialog1.findViewById(R.id.et_bucketname);
        TextView tv_body = (TextView) dialog1.findViewById(R.id.tv_body);
        LinearLayout cont_variables = (LinearLayout) dialog1.findViewById(R.id.cont_variables);
        et_bucketname.setText(msg.name);
        tv_body.setText(msg.getHighlightedBody());

        //Add Variable editors
        final ArrayList<String> variableNames = new ArrayList<>();
        int indexx = 0;
        for(String varname : msg.variables){
            variableNames.add(varname);
            final int index = 0+indexx++;
            final View view = mContext.getLayoutInflater().inflate(R.layout.bucket_editor_alertbox_item, cont_variables,false);
            final EditText et_varname = (EditText) view.findViewById(R.id.et_varname);
            et_varname.setText(varname);
            et_varname.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    variableNames.set(index, et_varname.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
            cont_variables.addView(view);
        }

        //Add Categories
        final int[] selectedCategoryIndex = {0};
        for(int i=0;i<categories.length();i++){
            try {
                if(msg.category.equals(categories.getString(i))){
                    selectedCategoryIndex[0] = i;
                }
            } catch (JSONException e) {e.printStackTrace();}
        }
        Spinner sp_category = (Spinner) dialog1.findViewById(R.id.sp_category);;
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_item, android.R.id.text1);
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_category.setAdapter(spinnerAdapter);
        for(int i=0;i<categories.length();i++){
            try {
                spinnerAdapter.add(categories.getString(i));
            } catch (JSONException e) {e.printStackTrace();}
        }
        sp_category.setSelection(selectedCategoryIndex[0]);
        spinnerAdapter.notifyDataSetChanged();
        sp_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCategoryIndex[0] = position;
                try {
                    if(categories.getString(selectedCategoryIndex[0]).equals("Transaction")){
                        dialog1.findViewById(R.id.ll_transactions).setVisibility(View.VISIBLE);
                    }else{
                        dialog1.findViewById(R.id.ll_transactions).setVisibility(View.GONE);
                    }
                } catch (JSONException e) {e.printStackTrace();}
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        //Add Vendors
        final int[] selectedVendorIndex = {0};
        for(int i=0;i<vendors.size();i++){
            if(msg.vendorId.equals(vendors.get(i).vendorId)){
                selectedVendorIndex[0] = i;
            }
        }
        Spinner sp_vendor = (Spinner) dialog1.findViewById(R.id.sp_vendor);;
        ArrayAdapter<String> spinnerAdapter2 = new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_item, android.R.id.text1);
        spinnerAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_vendor.setAdapter(spinnerAdapter2);
        for(int i=0;i<vendors.size();i++){
            spinnerAdapter2.add(""+vendors.get(i).vendorName);
        }
        sp_category.setSelection(selectedVendorIndex[0]);
        spinnerAdapter2.notifyDataSetChanged();
        sp_vendor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedVendorIndex[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //Transaction Extra Views
        try {
            if(categories.getString(selectedCategoryIndex[0]).equals("Transaction")){
                dialog1.findViewById(R.id.ll_transactions).setVisibility(View.VISIBLE);
            }else{
                dialog1.findViewById(R.id.ll_transactions).setVisibility(View.GONE);
            }
        } catch (JSONException e) {e.printStackTrace();}
        final boolean[] spent = {true};
        final RadioButton radio_spent = (RadioButton) dialog1.findViewById(R.id.radio_spent);
        RadioButton radio_earned = (RadioButton) dialog1.findViewById(R.id.radio_earned);
        View.OnClickListener radioListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spent[0] = radio_spent.isChecked();
            }
        };

        if(msg.extras.containsKey("spent")){
            boolean spentVar = (boolean) msg.extras.get("spent");
            spent[0] = spentVar;
            if(!spentVar) radio_earned.setChecked(true);
        }
        radio_spent.setOnClickListener(radioListener);
        radio_earned.setOnClickListener(radioListener);


        //Add Button Click listeners
        dialog1.findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = Router.Curate.editBucket();
                JSONObject req = new JSONObject();
                try {
                    req.put("_id", msg._id);
                    req.put("bucketname", et_bucketname.getText().toString());
                    JSONArray variables = new JSONArray();
                    for (String varname : variableNames) variables.put(varname);
                    req.put("variables", variables);
                    req.put("category", categories.getString(selectedCategoryIndex[0]));
                    req.put("vendorId", vendors.get(selectedVendorIndex[0]).vendorId);
                    if(categories.getString(selectedCategoryIndex[0]).equals("Transaction")){
                        JSONObject extras = new JSONObject();
                        extras.put("spent",spent[0]);
                        req.put("extras",extras);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                MainApplication.getInstance().addRequest(Request.Method.POST, url, req, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Logg.d(TAG, "Successfully edited : " + jsonObject.toString());
                        LocalBroadcastHandler.sendBroadcast(mContext, LocalBroadcastHandler.MESSAGES_UPLOADED);
                        dialog1.dismiss();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Logg.e(TAG, "ERROR : " + volleyError);
                        dialog1.dismiss();
                    }
                });

            }
        });

        dialog1.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.show();
    }

    static class ViewHolder {
        public TextView tv_header, tv_from, tv_body,tv_subheader;
    }
    private void deleteregex(BucketObj msg){
        String url = Router.Curate.deleteRegex(msg._id);
        MainApplication.getInstance().addRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Logg.d(TAG, "DELETE REGEX DATA : " + jsonObject.toString());
                LocalBroadcastHandler.sendBroadcast(mContext, LocalBroadcastHandler.MESSAGES_UPLOADED);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Logg.e(TAG, "ERROR : " + volleyError);
            }
        });
    }

}
