package com.maurice.grimreaper.grimreaper.Activities.Curate;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.maurice.grimreaper.grimreaper.Models.CurateSMSParser;
import com.maurice.grimreaper.grimreaper.Models.SMSMessage;
import com.maurice.grimreaper.grimreaper.R;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by maurice on 10/06/15.
 */
public class SP1_SelectMessageAdapter extends ArrayAdapter<SMSMessage> {
    Activity mContext;
    private final ArrayList<SMSMessage> SMSMessages;

    @Override
    public int getCount() {
        return SMSMessages.size();
    }

    public SP1_SelectMessageAdapter(Activity context, ArrayList<SMSMessage> SMSMessages){
        super(context, R.layout.smslist_list_item, SMSMessages);
        this.mContext = context;
        this.SMSMessages = SMSMessages;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = null;
        if (convertView == null) {
            LayoutInflater inflator = mContext.getLayoutInflater();
            view = inflator.inflate( R.layout.smslist_list_item, null );

            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.tv_header = (TextView) view.findViewById(R.id.tv_header);
            viewHolder.tv_body = (TextView) view.findViewById(R.id.tv_body);
            viewHolder.tv_body2 = (TextView) view.findViewById(R.id.tv_body2);

            view.setTag(viewHolder);
        } else {
            view = convertView;
        }

        final SMSMessage msg = SMSMessages.get(position);
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.tv_header.setText(msg.number);
        holder.tv_body.setText(msg.body);
        long time = Long.parseLong(msg.date);
//        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("MMMM d h:mm a");
//        System.out.println(format.format(calendar.getTime()));
        holder.tv_body2.setText(format.format(new Date(time)));

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,SP2_SelectScanners.class);
                CurateSMSParser curateSMSParser = new CurateSMSParser();
                curateSMSParser.sampleMsg = msg;
                intent.putExtra(SP2_SelectScanners.MESSAGE, curateSMSParser);
                mContext.startActivityForResult(intent,200);
            }
        });

        return view;
    }

    static class ViewHolder {
        public TextView tv_header, tv_body, tv_body2;
    }

}
