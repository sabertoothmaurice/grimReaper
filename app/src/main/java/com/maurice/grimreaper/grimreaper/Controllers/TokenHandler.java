package com.maurice.grimreaper.grimreaper.Controllers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.maurice.grimreaper.grimreaper.Activities.Main.MainActivity;
import com.maurice.grimreaper.grimreaper.MainApplication;
import com.maurice.grimreaper.grimreaper.Utils.Logg;
import com.maurice.grimreaper.grimreaper.Utils.Router;
import com.maurice.grimreaper.grimreaper.Utils.Settings;
import com.maurice.grimreaper.grimreaper.storage.SharedPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * This contains all the User data excluding kids,
 */
public class TokenHandler {
    static String TAG = "TOKENHANDLER";
    private Context mContext;
    private static TokenHandler instance;
    private SharedPrefs sPrefs;
    public String name;

    public static String AUTH_FACEBOOK = "facebook";
    public static String AUTH_GOOGLE = "google";
    public static String AUTH_NONE = "none";


    public String email;//this needs to be stored so that google token update can work seamlessly

    //actual tracked variables
    public String authProvider = AUTH_NONE;
    public String servertoken;//this is clozerr service token
    public String socialtoken;//this is token of facebook/google


    private GoogleApiClient finalMGoogleApiClient;//used when logging out


    private TokenHandler(Context context) {
        mContext = context;
        pullTokenDataFromLocal();
        if(hasSocialToken()){
            updateLoginTokens();
        }
    }
    public static TokenHandler getInstance(Context context) {
        if(instance == null) {
            instance = new TokenHandler(context);
        }
        return instance;
    }


    //LOCAL STORAGE ENCODERS
    public void pullTokenDataFromLocal() {
        sPrefs = SharedPrefs.getInstance(mContext);
        try {
            authProvider = (sPrefs.loginData.has("authProvider"))?sPrefs.loginData.getString("authProvider"):AUTH_NONE;
            socialtoken = (sPrefs.loginData.has("socialtoken"))?sPrefs.loginData.getString("socialtoken"):"";
            servertoken = (sPrefs.loginData.has("servertokenD"))?sPrefs.loginData.getString("servertokenD"):"";
            email = (sPrefs.loginData.has("email"))?sPrefs.loginData.getString("email"):"";
        } catch (JSONException e) {e.printStackTrace();}
    }
    public void saveTokenDataLocally() {
        try {
            sPrefs.loginData.put("authProvider", authProvider);
            sPrefs.loginData.put("servertokenD", servertoken);
            sPrefs.loginData.put("socialtoken", socialtoken);
            sPrefs.loginData.put("email", email);
        } catch (JSONException e) {e.printStackTrace();}
        sPrefs.saveLoginData();
    }


    /** SOME PUBLIC GET FUNCTIONS */
    public boolean hasToken(){
        return (!servertoken.isEmpty());
    }
    public boolean isLoggedIn() {
        return (!servertoken.isEmpty());
    }
    public boolean hasSocialToken(){
        return (!socialtoken.isEmpty());
    }
    public boolean updateToken(){
        return (!servertoken.isEmpty());
    }
    public boolean loggedByFb(){
        return (authProvider.equals(AUTH_FACEBOOK));
    }
    public boolean loggedByGoogle(){
        return (authProvider.equals(AUTH_GOOGLE));
    }

    /** SOME PUBLIC PUT FUNCTIONS */

    public void addSocialToken(String token, String authProviderStr){
        socialtoken = token;
        authProvider = authProviderStr;
        saveTokenDataLocally();
    }

    public void getServerToken(final ServerTokenListener listener){
        Logg.d(TAG,"fetching Server Token....");
//        String authGuy = (authProvider.equals(AUTH_GOOGLE))?"google":"facebook";
        String url = Router.User.googlelogin(socialtoken,email);
        MainApplication.getInstance().addRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Logg.d(TAG, "Received response for Server Token");
                    if (response.getInt("statusCode") == 200) {
                        JSONObject result = response.getJSONObject("result");
                        JSONObject user = result.getJSONObject("user");
                        email = user.getString("email");
                        servertoken = user.getString("access_token");

                        //Overrides
                        if(Settings.completeOverride){
                            servertoken = Settings.completeOverride_accesstoken;
                            email = Settings.completeOverride_email;
                        }

                        ToastMain.showSmartToast(mContext, "Got Server Token : " + servertoken);
                        saveTokenDataLocally();
                        listener.onServerTokenreceived(true);
                    } else {
                        ToastMain.showSmartToast(mContext, "Invalid response code : "+response);
                        listener.onServerTokenreceived(false);
                    }
                } catch (JSONException e) {
                    Logg.e(TAG, "error in fetching Server token");
                    ToastMain.showSmartToast(mContext, "error in fetching Server token :1243");
                    e.printStackTrace();
                    listener.onServerTokenreceived(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onServerTokenreceived(false);
                ToastMain.showSmartToast(mContext, "error in fetching Server token : 35412");
                Log.d("ERROR" + TAG, "Error in getting Server token 1231" + error.getLocalizedMessage());
            }
        });
    }

    private void updateLoginTokens(){
        AsyncTask task = new AsyncTask() {
            @Override
            protected Object doInBackground(Object... params) {
                try {
                    if (authProvider.equals(AUTH_GOOGLE)) {
                        servertoken = GoogleAuthUtil.getToken(mContext, email, "oauth2:" + Scopes.PLUS_LOGIN);
                        saveTokenDataLocally();
                    }else{
//                        AccessToken tokenFb = AccessToken.getCurrentAccessToken();
//                        clozerrtokenS = tokenFb.getToken();
//                        saveTokenDataLocally();
                    }
                }catch (RuntimeException | GoogleAuthException | IOException e) {e.printStackTrace();}
                return null;
            }
        };
        task.execute((Void) null);

    }

    public interface ServerTokenListener {
        void onServerTokenreceived(boolean isUpdated);
    }

    public void print(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("authProvider",""+authProvider);
            obj.put("clozerrtokenD",""+ servertoken);
            obj.put("socialtoken", "" + socialtoken);
            obj.put("email", "" + email);
        } catch (JSONException e) {e.printStackTrace();}
    }

    /** LOGOUT STACKS */

    /**
     * This is the main function that has to be called in order to completely logout
     *
     * @param activity
     */
    public void logout(Activity activity) {
        Logg.d(TAG, "Logout procedure started...");
        print();
        if(loggedByGoogle())logoutGoogle(activity);
        if(loggedByFb())logoutFacebook(activity);
        //It never reaches here as above method itself calls the callbacks to exit app
    }
    private void logoutGoogle(final Activity activity){
        //Logout from google
        Logg.e(TAG,"Started Google Logout Procedure");
        finalMGoogleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle bundle) {
                        googleLogoutCallback(activity);
                        clearCacheData();
                        Logg.e(TAG, "Successfully logged out from google");
                        Logg.d(TAG, "restarting...");

                        //FInally kill the calling activity and restart app
                        activity.startActivity(new Intent(activity, MainActivity.class));
                        activity.finish();
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        ToastMain.showSmartToast(activity, "Error logging out","Error : google logout : onConnectionSuspended");
                    }
                })
                .addApi(Plus.API)
                .addScope(new Scope(Scopes.PROFILE))
                .build();
        finalMGoogleApiClient.connect();
    }

    private void googleLogoutCallback(Context context) {
        if (finalMGoogleApiClient != null) {
            if (finalMGoogleApiClient.isConnected()) {
                Plus.AccountApi.clearDefaultAccount(finalMGoogleApiClient);
                finalMGoogleApiClient.disconnect();
                ToastMain.showSmartToast(context, "Successfully logged out of Google");
                return;
            }
        }
        ToastMain.showSmartToast(context, "Error in logging out of google", "Error, google logout error : 301");
    }

    private void logoutFacebook(Activity activity) {
        clearCacheData();
//        LoginManager.getInstance().logOut();
        Logg.e(TAG, "Successfully logged out from Facebook");
        Logg.d(TAG, "restarting...");
        //FInally kill the calling activity and restart app
        activity.startActivity(new Intent(activity, MainActivity.class));
        activity.finish();
    }
    private void clearCacheData(){
        servertoken = "";
        email = "";
        socialtoken = "";
        authProvider = AUTH_NONE;
        saveTokenDataLocally();
    }
}
