package com.maurice.grimreaper.grimreaper.Models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

public class TransactionObj implements Comparable<TransactionObj> {

    public int cost;
    public String vendorId;
    public Long time;
    public String vendorName;
    public String vendorImage,fromVendorName;
    public ArrayList<String> sourceStrings = new ArrayList<>();
    public HashMap<String,String> data = new HashMap<>();

    public TransactionObj() {
    }

    //SERVER ENCODERS
    public static TransactionObj decode(JSONObject obj){

        TransactionObj smsMessage = new TransactionObj();
        try {
            smsMessage.cost = (obj.has("cost")) ? obj.getInt("cost") : 0;
            JSONObject dataJSON = (obj.has("data")) ? obj.getJSONObject("data") : new JSONObject();
            Iterator<?> keys = dataJSON.keys();
            while( keys.hasNext()) {
                String key = (String) keys.next();
                if (dataJSON.get(key) instanceof String ) {
                    if(key.equals("cost")) continue;
                    smsMessage.data.put(key,dataJSON.getString(key));
                }
            }

            smsMessage.vendorId = (obj.has("vendorId")) ? obj.getString("vendorId") : null;
            smsMessage.time = (obj.has("time")) ? obj.getLong("time") : Calendar.getInstance().getTimeInMillis();
            smsMessage.vendorName = (obj.has("vendorName")) ? obj.getString("vendorName") : null;
            smsMessage.vendorImage = (obj.has("vendorImage")) ? obj.getString("vendorImage") : null;

            JSONArray sourceArr = (obj.has("source")) ? obj.getJSONArray("source") : new JSONArray();
            for(int i=0; i<sourceArr.length();i++){
                smsMessage.sourceStrings.add(sourceArr.getString(i));
            }

            JSONObject from = (obj.has("from")) ? obj.getJSONObject("from") : new JSONObject();
            smsMessage.fromVendorName = (from.has("name")) ? from.getString("name") : "";
        } catch (JSONException e) {e.printStackTrace();}
        return smsMessage;
    }

    public static ArrayList<TransactionObj> decode(JSONArray obj){

        ArrayList<TransactionObj> list = new ArrayList<>();
        for(int i=0;i<obj.length();i++){
            try {
                list.add(decode(obj.getJSONObject(i)));
            } catch (JSONException e) {e.printStackTrace();}
        }
        return list;
    }

    public JSONObject encode(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put( "cost", cost );
            jsonObject.put( "vendorId", vendorId );
            jsonObject.put( "time", time );
            jsonObject.put( "vendorName", vendorName );
            jsonObject.put( "vendorImage", vendorImage );

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public int compareTo(TransactionObj another) {
        return 0;//TODO : implement this
    }
}
