package com.maurice.grimreaper.grimreaper.Activities.Login;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.astuetz.PagerSlidingTabStrip;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.maurice.grimreaper.grimreaper.Activities.Main.MainActivity;
import com.maurice.grimreaper.grimreaper.Controllers.TokenHandler;
import com.maurice.grimreaper.grimreaper.MainApplication;
import com.maurice.grimreaper.grimreaper.Models.UserMain;
import com.maurice.grimreaper.grimreaper.R;
import com.maurice.grimreaper.grimreaper.Utils.Logg;
import com.maurice.grimreaper.grimreaper.Utils.Router;
import com.maurice.grimreaper.grimreaper.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

public class LoginActivity extends FragmentActivity{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;


    /** LOGIN */
    /* Is there a ConnectionResult resolution in progress? */
    private boolean mIsResolving = false;
    /* Should we automatically resolve ConnectionResults when possible? */
    private boolean mShouldResolve = false;
    /* Request code used to invoke sign in user interactions. */
    private static final int RC_SIGN_IN = 0;
    /* Client used to interact with Google APIs. */
    private GoogleApiClient mGoogleApiClient;

    //Generic Variables
    static final String TAG = "LOGINACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if(getActionBar()!=null) getActionBar().hide();

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // Bind the tabs to the ViewPager
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(mViewPager);


        //Bind Google Login
        findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tryLoggingIn();
            }
        });

        //find account information
        findAccountInformation();

    }

    private void findAccountInformation() {
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(MainApplication.getInstance()).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                String possibleEmail = account.name;
                Utils.showDebugToast(this,"Possible : "+possibleEmail);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: return LoginFragment.newInstance(R.layout.fragment_login_page1);
                case 1: return LoginFragment.newInstance(R.layout.fragment_login_page2);
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0: return "LOGIN";
                case 1: return "SIGNUP";
            }
            return null;
        }
    }


    private void googleLoginServer(String accessToken){
        String url = Router.User.googlelogin(accessToken,"email");
        Logg.d(TAG, "Signup : " + url);
        MainApplication.getInstance().addRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Utils.showDebugToast(LoginActivity.this, "Logged in as : " + jsonObject.toString());
                Logg.d(TAG, "USER DATA : " + jsonObject.toString());
                try {
                    UserMain user = MainApplication.getInstance().data.userMain;
                    user.decodeFromServer(jsonObject.getJSONObject("result"));
                    user.saveUserDataLocally();
                    tryLoggingIn();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Logg.e(TAG, "ERROR : " + volleyError);
                Utils.showDebugToast(LoginActivity.this, volleyError.networkResponse.statusCode + " : Some error has occurred");
            }
        });
    }
    public void tryLoggingIn(){




        // Google login callback
        AsyncTask task = new AsyncTask() {
            @Override
            protected Object doInBackground(Object... params) {
                try {
                    if (Plus.AccountApi.getAccountName(mGoogleApiClient) != null) {
                        String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                        String scope = "oauth2:" + Scopes.PLUS_LOGIN;
//                        String token = GoogleAuthUtil.getToken(LoginActivity.this.getApplicationContext(), email, scope);
                        TokenHandler tokenHandler = MainApplication.getInstance().tokenHandler;
//                        tokenHandler.socialtoken = token;
                        tokenHandler.authProvider = TokenHandler.AUTH_GOOGLE;
                        tokenHandler.email = email;
                        tokenHandler.saveTokenDataLocally();
                        tokenHandler.getServerToken(new TokenHandler.ServerTokenListener() {
                            @Override
                            public void onServerTokenreceived(boolean isReceived) {
                                if(isReceived){
                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                                    intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(intent);
                                    finish();
                                }else{

                                }
                            }
                        });
                    } else {
                        Utils.showDebugToast(getApplicationContext(), "Person information is null");
                    }
                } catch (Exception e) {e.printStackTrace();}
                return null;
            }
        };
        task.execute((Void) null);
//        UserMain user = MainApplication.getInstance().data.userMain;
//        if(user.userId!=null&&!user.userId.isEmpty()){
//            Intent intent = new Intent(this, MapsActivity.class);
//            startActivity(intent);
//        }
    }



}
