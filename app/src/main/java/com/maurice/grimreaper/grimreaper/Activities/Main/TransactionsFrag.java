package com.maurice.grimreaper.grimreaper.Activities.Main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.maurice.grimreaper.grimreaper.Controllers.LocalBroadcastHandler;
import com.maurice.grimreaper.grimreaper.MainApplication;
import com.maurice.grimreaper.grimreaper.Models.TransactionObj;
import com.maurice.grimreaper.grimreaper.R;
import com.maurice.grimreaper.grimreaper.Utils.Logg;
import com.maurice.grimreaper.grimreaper.Utils.Router;
import com.maurice.grimreaper.grimreaper.storage.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * This is the main fragment user for listing user Notifications
 */
public class TransactionsFrag extends android.support.v4.app.Fragment {
    static final String TAG = "TransactionsFrag";
    public Context mContext;
    ArrayList<TransactionObj> allSMSMessages = new ArrayList<>();
    ListView notificationsLV;
    SharedPrefs sharedPrefs = SharedPrefs.getInstance(getActivity());
    SwipeRefreshLayout refresh_cont;

    private BroadcastReceiver notificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("notificationReceiver: ", " : FORUM_NOTIFICATION_RECEIVED");
            // TODO // Show a fading background for the notification that just came in real time via this broadcast
            fetchData();
        }
    };

    public TransactionsFrag() {
    }

    public static TransactionsFrag newInstance(Context activityContext) {
        TransactionsFrag myFragment = new TransactionsFrag();
        myFragment.mContext = activityContext;
        return myFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Listen to GCM Notifications all the while
        LocalBroadcastManager.getInstance(mContext).registerReceiver(notificationReceiver,
                new IntentFilter("FORUM_NOTIFICATION_RECEIVED"));
    }


    private void fetchData(){
        String url = Router.Data.transactions();
        MainApplication.getInstance().addRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Logg.d(TAG, "USER DATA : " + jsonObject.toString());
                try {
                    JSONObject result = jsonObject.getJSONObject("result");
                    JSONArray transactionsArr = result.has("transactionsArr") ? result.getJSONArray("transactionsArr") : new JSONArray();

                    allSMSMessages.clear();
                    allSMSMessages.addAll(TransactionObj.decode(transactionsArr));
                    if (notificationsLV.getAdapter() == null) {
                        notificationsLV.setAdapter(new TransactionsFragAdapter(getActivity(), allSMSMessages));
                    }

                    refresh_cont.setRefreshing(false);
                    ((BaseAdapter) notificationsLV.getAdapter()).notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Logg.e(TAG, "ERROR : " + volleyError);
                refresh_cont.setRefreshing(false);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        // Register mMessageReceiver to receive messages.
        LocalBroadcastManager.getInstance(mContext).registerReceiver(notificationReceiver, new IntentFilter(LocalBroadcastHandler.MESSAGES_UPLOADED));

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView;
        rootView = inflater.inflate(R.layout.fragment_transactions, null);

        notificationsLV = (ListView) rootView.findViewById(R.id.notificationsLV);

        refresh_cont = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh_cont);
        refresh_cont.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchData();
            }
        });
        fetchData();
        return rootView;
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        for(int i=0;i<menu.size();i++){
            menu.getItem(i).setVisible(false);
        }
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver( notificationReceiver );
        super.onPause();
    }
}
