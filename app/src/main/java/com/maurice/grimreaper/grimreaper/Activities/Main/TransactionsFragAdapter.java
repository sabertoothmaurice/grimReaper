package com.maurice.grimreaper.grimreaper.Activities.Main;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.maurice.grimreaper.grimreaper.Models.TransactionObj;
import com.maurice.grimreaper.grimreaper.R;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by maurice on 10/06/15.
 */
public class TransactionsFragAdapter extends ArrayAdapter<TransactionObj> {
    Activity mContext;
    private final ArrayList<TransactionObj> transactions;

    @Override
    public int getCount() {
        return transactions.size();
    }

    public TransactionsFragAdapter(Activity context, ArrayList<TransactionObj> transactions){
        super(context, R.layout.transactions_list_item, transactions);
        this.mContext = context;
        this.transactions = transactions;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = null;
        if (convertView == null) {
            LayoutInflater inflator = mContext.getLayoutInflater();
            view = inflator.inflate(R.layout.transactions_list_item, null);

            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.main_cont = view.findViewById(R.id.main_cont);
            viewHolder.header = view.findViewById(R.id.header);
            viewHolder.tv_expenses = (TextView) view.findViewById(R.id.tv_expenses);
            viewHolder.tv_head_month = (TextView) view.findViewById(R.id.tv_head_month);
            viewHolder.tv_transactions = (TextView) view.findViewById(R.id.tv_transactions);


            viewHolder.tv_header = (TextView) view.findViewById(R.id.tv_header);
            viewHolder.tv_header_right = (TextView) view.findViewById(R.id.tv_header_right);
            viewHolder.tv_subheader = (TextView) view.findViewById(R.id.tv_subheader);
            viewHolder.tv_cost = (TextView) view.findViewById(R.id.tv_cost);
            viewHolder.tv_day = (TextView) view.findViewById(R.id.tv_day);
            viewHolder.tv_month = (TextView) view.findViewById(R.id.tv_month);


            view.setTag(viewHolder);
        } else {
            view = convertView;
        }


        final TransactionObj msg = transactions.get(position);
        final ViewHolder holder = (ViewHolder) view.getTag();
        if(isMonthStart(position)){
            holder.header.setVisibility(View.VISIBLE);
            holder.tv_expenses.setText("" + getExpensesForMonth(msg.time));
            holder.tv_transactions.setText("" + getTrasactionsForMonth(msg.time));
            holder.tv_head_month.setText((new SimpleDateFormat("MMMM")).format(new Date(msg.time)));
        }else{
            holder.header.setVisibility(View.GONE);
        }


        holder.tv_header.setText(msg.vendorName);
        String subheader = "";
        for(String key : msg.data.keySet()){
            subheader += msg.data.get(key) + "  ";
        }
        holder.tv_subheader.setText("" + subheader);
        String right_subheader = (!msg.fromVendorName.isEmpty())?"(transferred from "+msg.fromVendorName+")":"";
        holder.tv_header_right.setText(right_subheader);
        holder.tv_cost.setText("" + msg.cost);
        holder.tv_day.setText((new SimpleDateFormat("d")).format(new Date(msg.time)));
        holder.tv_month.setText((new SimpleDateFormat("MMM")).format(new Date(msg.time)));
        holder.main_cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSourceInAlert(msg.sourceStrings);
            }
        });

        return view;
    }

    private void showSourceInAlert(ArrayList<String> sourceStrings) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(mContext);
        builderSingle.setIcon(R.drawable.common_signin_btn_icon_disabled_dark);
        builderSingle.setTitle("Source messages");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(mContext,
                android.R.layout.select_dialog_item);
        for(String msg : sourceStrings){
            arrayAdapter.add(msg);
        }

        builderSingle.setAdapter(
                arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        builderSingle.show();
    }

    private int getExpensesForMonth(long time){
        int expenses = 0;
        for(TransactionObj transaction : transactions){
            if(isSameMonth(time,transaction.time)&&isSameYear(time,transaction.time)){
                expenses += transaction.cost;
            }
        }
        return expenses;
    }
    private int getTrasactionsForMonth(long time){
        int transactions = 0;
        for(TransactionObj transaction : this.transactions){
            if(isSameMonth(time,transaction.time)&&isSameYear(time,transaction.time)){
                transactions += 1;
            }
        }
        return transactions;
    }

    private boolean isMonthStart(int position){
        if(position==0) return true;
        TransactionObj msg = transactions.get(position);
        TransactionObj msgPrev = transactions.get(position-1);
        return (!getMonth(msg).equals(getMonth(msgPrev)));
    }

    private String getMonth(TransactionObj msg){
        return (new SimpleDateFormat("MMM")).format(new Date(msg.time));
    }
    private String getYear(TransactionObj msg){
        return (new SimpleDateFormat("yyyy")).format(new Date(msg.time));
    }
    private boolean isSameMonth(long time1, long time2){
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(time1);
        int month1 = cal1.get(Calendar.MONTH);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTimeInMillis(time2);
        int month2 = cal2.get(Calendar.MONTH);
        return month1==month2;
    }
    private boolean isSameYear(long time1, long time2){
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(time1);
        int year1 = cal1.get(Calendar.YEAR);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTimeInMillis(time2);
        int year2 = cal2.get(Calendar.YEAR);
        return year1==year2;
    }

    static class ViewHolder {
        public TextView tv_header, tv_day, tv_month,tv_cost,tv_subheader;
        public View header;
        public TextView tv_expenses;
        public TextView tv_transactions;
        public TextView tv_head_month;
        public TextView tv_header_right;
        public View main_cont;
    }

}
