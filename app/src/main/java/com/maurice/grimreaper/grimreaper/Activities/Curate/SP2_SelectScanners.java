package com.maurice.grimreaper.grimreaper.Activities.Curate;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.maurice.grimreaper.grimreaper.MainApplication;
import com.maurice.grimreaper.grimreaper.Models.CurateSMSParser;
import com.maurice.grimreaper.grimreaper.Models.SMSMessage;
import com.maurice.grimreaper.grimreaper.R;
import com.maurice.grimreaper.grimreaper.Utils.Utils;
import com.wefika.flowlayout.FlowLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SP2_SelectScanners extends AppCompatActivity {

    public static final String MESSAGE = "MESSAGE";

    CurateSMSParser curateSMSParser;
    FlowLayout flowCont;
    LayoutInflater inflator;
    ArrayList<String> bodyAsArrStr = new ArrayList<>();
    ArrayList<Integer> selected = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sp2__select_scanners);
        curateSMSParser = (CurateSMSParser) getIntent().getSerializableExtra(MESSAGE);
        inflator = getLayoutInflater();
        bodyAsArrStr = curateSMSParser.sampleMsg.returnBodyAsArrStr();

        flowCont = (FlowLayout) findViewById(R.id.flowCont);

        findViewById(R.id.btn_test).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMatchedMessagesInAlert();
            }
        });

        findViewById(R.id.btn_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SP2_SelectScanners.this,SP3_SelectParsers.class);
                intent.putExtra(SP2_SelectScanners.MESSAGE, curateSMSParser);
                startActivityForResult(intent, 200);
            }
        });

        generateButtonsUIFromMessage();
    }


    private void generateButtonsUIFromMessage(){
        int index = 0;
        flowCont.removeAllViews();
        for(String str : bodyAsArrStr){
            View cont = inflator.inflate(R.layout.sp2_button, flowCont, false);
            TextView textView = (TextView) cont.findViewById(R.id.tv_main);
            if(selectedHasIndex(index)){
                textView.setBackgroundColor(Color.GREEN);
            }else{
                textView.setBackgroundColor(Color.RED);
            }


            textView.setText(str);
            final int strFinalIndex = 0+index;
            cont.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleString(strFinalIndex);
                    generateButtonsUIFromMessage();
                    curateSMSParser.matchers = getSelectedStrings();
                }
            });
            flowCont.addView(cont);
            index++;
        }
    }

    private void toggleString(int toggledIndex) {
        int index = 0;
        for(Integer strIndex : selected){
            if(toggledIndex==strIndex){
                //deactivate this
                selected.remove(index);
                return;
            }
            index++;
        }

        //activate this
        selected.add(toggledIndex);

    }

    public boolean selectedHasIndex(int index){
        for(Integer strIndex : selected){
            if(index==strIndex){
                return true;
            }
        }
        return false;
    }

    public ArrayList<String> getSelectedStrings(){
        ArrayList<String> result = new ArrayList<>();
        int beforeIndex = -10;
        Collections.sort(selected,new IntegerComparator());
        for(Integer strIndex : selected){
            if(strIndex-beforeIndex==1){
                result.set(result.size()-1,result.get(result.size()-1)+" "+bodyAsArrStr.get(strIndex));
            }else{
                result.add(bodyAsArrStr.get(strIndex));
            }
            beforeIndex = strIndex;
        }
        return result;
    }

    public class IntegerComparator implements Comparator<Integer> {
        public IntegerComparator() {}

        @Override
        public int compare(Integer i1, Integer i2) {
            return i1 < i2 ? -1 : (i1 == i2 ? 0 : 1);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sp2__select_scanners, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showMatchedMessagesInAlert(){
        final Dialog dialog1 = new Dialog(this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.sp2_alertbox_test);
        ListView listView = (ListView) dialog1.findViewById(R.id.listView);
        final ArrayAdapter<Spanned> arrayAdapter = new ArrayAdapter<>(this,
                R.layout.sp2_alertbox_item);

        dialog1.findViewById(R.id.btn_yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });


        ArrayList<String> selected = getSelectedStrings();
        ArrayList<SMSMessage> smsDump = MainApplication.getInstance().smsDumpHandler.smsRawDump;
        for(SMSMessage msg : smsDump){
            if(selected.size()==0) break;
            boolean passed = true;
            for(String text : selected){
                if(!msg.body.contains(text)){
                    passed = false;
                    break;
                }
            }

            if(passed){
                arrayAdapter.add(Utils.highlightText(selected, msg.body));
            }
        }

        listView.setAdapter(arrayAdapter);


        dialog1.show();
    }
}
