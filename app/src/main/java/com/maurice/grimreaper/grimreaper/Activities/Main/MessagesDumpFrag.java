package com.maurice.grimreaper.grimreaper.Activities.Main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.maurice.grimreaper.grimreaper.MainApplication;
import com.maurice.grimreaper.grimreaper.Models.SMSDirectory;
import com.maurice.grimreaper.grimreaper.R;
import com.maurice.grimreaper.grimreaper.storage.SharedPrefs;

import java.util.ArrayList;
import java.util.List;

/**
 * This is the main fragment user for listing user Notifications
 */
public class MessagesDumpFrag extends android.support.v4.app.Fragment {

    public Context mContext;
    ArrayList<SMSDirectory> allSMSDirectories = new ArrayList<>();
    ListView notificationsLV;
    SharedPrefs sharedPrefs = SharedPrefs.getInstance(getActivity());
    SwipeRefreshLayout refresh_cont;

    private BroadcastReceiver notificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("notificationReceiver: ", " : FORUM_NOTIFICATION_RECEIVED");
            // TODO // Show a fading background for the notification that just came in real time via this broadcast
            setAdapterFromLocalNotifications();
        }
    };

    public MessagesDumpFrag() {
    }

    public static MessagesDumpFrag newInstance(Context activityContext) {
        MessagesDumpFrag myFragment = new MessagesDumpFrag();
        myFragment.mContext = activityContext;
        return myFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Listen to GCM Notifications all the while
        LocalBroadcastManager.getInstance(mContext).registerReceiver(notificationReceiver,
                new IntentFilter("FORUM_NOTIFICATION_RECEIVED"));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView;
        rootView = inflater.inflate(R.layout.fragment_notifications, null);

        notificationsLV = (ListView) rootView.findViewById(R.id.notificationsLV);

        refresh_cont = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh_cont);
        refresh_cont.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                MainApplication.getInstance().smsDumpHandler.syncDumpInServer();
                setAdapterFromLocalNotifications();
            }
        });

        setAdapterFromLocalNotifications();
        return rootView;
    }

    public List<SMSDirectory> setAdapterFromLocalNotifications() {

        allSMSDirectories.clear();
        allSMSDirectories.addAll(MainApplication.getInstance().smsDumpHandler.smsDirectories);
        if( notificationsLV.getAdapter() == null ){
            notificationsLV.setAdapter(new MessagesDumpFragAdapter(getActivity(), allSMSDirectories));
        }

        refresh_cont.setRefreshing(false);
        ((BaseAdapter) notificationsLV.getAdapter()).notifyDataSetChanged();

        return allSMSDirectories;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        for(int i=0;i<menu.size();i++){
            menu.getItem(i).setVisible(false);
        }
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver( notificationReceiver );
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
    }

}

