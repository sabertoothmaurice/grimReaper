package com.maurice.grimreaper.grimreaper.Activities.Main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.maurice.grimreaper.grimreaper.MainApplication;
import com.maurice.grimreaper.grimreaper.Models.SMSDirectory;
import com.maurice.grimreaper.grimreaper.R;

public class SMSDirectoryActivity extends AppCompatActivity {
    static final String INTENT_SMS_FROM = "INTENT_SMS_FROMD";

    String from;
    SMSDirectory directory;
    ListView lv_messages;
    View btn_back;
    TextView tv_actionbar_header;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smsdirectory);
        if(getSupportActionBar()!=null) getSupportActionBar().hide();

        from = getIntent().getStringExtra(INTENT_SMS_FROM);

        lv_messages = (ListView) findViewById(R.id.lv_messages);
        tv_actionbar_header = (TextView) findViewById(R.id.tv_actionbar_header);
        tv_actionbar_header.setText(from);
        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        directory = MainApplication.getInstance().smsDumpHandler.getSmsDirectory(from);
        fillData();
    }

    private void fillData(){
        if( lv_messages.getAdapter() == null ){
            lv_messages.setAdapter(new SMSDirectoryActivityAdapter(this, directory.messages));
        }

        ((BaseAdapter) lv_messages.getAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
