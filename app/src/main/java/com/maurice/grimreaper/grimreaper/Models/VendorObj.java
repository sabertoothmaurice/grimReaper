package com.maurice.grimreaper.grimreaper.Models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VendorObj implements Comparable<VendorObj> {

    public String vendorId;
    public String vendorName;
    public String vendorImage;

    public VendorObj() {
    }

    //SERVER ENCODERS
    public static VendorObj decode(JSONObject obj){

        VendorObj SMSMessage = new VendorObj();
        try {
            SMSMessage.vendorId = (obj.has("_id")) ? obj.getString("_id") : null;
            SMSMessage.vendorName = (obj.has("name")) ? obj.getString("name") : null;
            SMSMessage.vendorImage = (obj.has("image")) ? obj.getString("image") : null;
        } catch (JSONException e) {e.printStackTrace();}
        return SMSMessage;
    }

    public static ArrayList<VendorObj> decode(JSONArray obj){

        ArrayList<VendorObj> list = new ArrayList<>();
        for(int i=0;i<obj.length();i++){
            try {
                list.add(decode(obj.getJSONObject(i)));
            } catch (JSONException e) {e.printStackTrace();}
        }
        return list;
    }

    public JSONObject encode(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put( "vendorId", vendorId );
            jsonObject.put( "vendorName", vendorName );
            jsonObject.put( "vendorImage", vendorImage );

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public int compareTo(VendorObj another) {
        return 0;//TODO : implement this
    }
}
