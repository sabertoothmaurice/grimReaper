package com.maurice.grimreaper.grimreaper.Views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.XAxisValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.maurice.grimreaper.grimreaper.Models.CardObj;
import com.maurice.grimreaper.grimreaper.R;
import com.maurice.grimreaper.grimreaper.Utils.Logg;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * This is basic builder for getting a question view which can be used to inflate Question in Forum, ProfileScreen etc
 */
public class BankCardViewBuilder {
    static final String TAG = "BANKCARDVIEW";

    public static View getBankCardView(Activity activity){
        LayoutInflater inflator = LayoutInflater.from(activity);
        View mainView = inflator.inflate(R.layout.mycards_list_item, null);
        ViewHolder holder = new ViewHolder(mainView, activity);
        mainView.setTag(holder);
        return mainView;
                
    }

    public static class ViewHolder{
        public View mainView;
        public TextView tv_header, tv_subheader, tv_body,tv_cost;
        public ImageView iv_left;
        public TextView tv_acc_1,tv_acc_2,tv_acc_3,tv_acc_4;
        public LineChart chart_balance, chart_transactions;
        public Context mContext;

        public ViewHolder(View view, Activity activity) {
            mContext = activity;
            mainView = view;
            tv_header = (TextView) view.findViewById(R.id.tv_header);
            tv_subheader = (TextView) view.findViewById(R.id.tv_subheader);
            chart_balance = (LineChart) view.findViewById(R.id.chart_balance);
            chart_transactions = (LineChart) view.findViewById(R.id.chart_transactions);
        }


        // TODO - WTF! UserActivityObject and ActivityObject classes?! use ONE dude!

        public void inflateData(final CardObj msg){
            Logg.d(TAG, "Inflating data in Bank Card view");

            tv_header.setText(msg.cardName);
            tv_subheader.setText(msg.vendorName);
//        ImageLoader.getInstance().displayImage(msg.logoUrl, holder.iv_left, options_map);


            //Set Balance chart
            setBalancesChart(msg.balances);

            //Set Transactions chart
            setTransactionsChart(msg.transactions);

        }

        private void setBalancesChart(List<CardObj.Balance> balances){
            LineChart chart = chart_balance;
            ArrayList<Entry> valsComp1 = new ArrayList<>();
            ArrayList<String> xVals = new ArrayList<>();
            int i=0;
            for(CardObj.Balance balance : balances){
                Entry c1e1 = new Entry((float) balance.balance, i++);
                valsComp1.add(c1e1);
                xVals.add(balance.getDaysAgo()+"");
            }

            LineDataSet set1 = new LineDataSet(valsComp1, "");
            set1.setDrawCubic(true);
            set1.setCubicIntensity(0.2f);
            set1.setColor(Color.WHITE);
            set1.setCircleColor(Color.WHITE);
            set1.setLineWidth(3f);
            set1.setCircleSize(2f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(9f);
            set1.setFillAlpha(65);
            set1.setFillColor(mContext.getResources().getColor(R.color.ColorPrimary));

            chart.getLegend().setEnabled(false);
            chart.getAxisRight().setEnabled(false);
            YAxis yAxis = chart.getAxisLeft();
            yAxis.setLabelCount(3, false);
            yAxis.setTextColor(Color.WHITE);
            yAxis.setGridColor(mContext.getResources().getColor(R.color.ColorPrimaryLight));
            yAxis.setAxisLineColor(mContext.getResources().getColor(R.color.ColorPrimaryLight));

            XAxis xAxis = chart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setTextSize(10f);
            xAxis.setTextColor(Color.WHITE);
            xAxis.setDrawAxisLine(true);
            yAxis.setLabelCount(4, false);
            xAxis.setDrawGridLines(false);
            xAxis.setAxisLineColor(mContext.getResources().getColor(R.color.ColorPrimaryLight));
            xAxis.setValueFormatter(new MyCustomXAxisValueFormatter());

            chart.setDescription("");
            chart.setBackgroundColor(Color.TRANSPARENT);
            chart.setBorderColor(Color.TRANSPARENT);
            chart.setGridBackgroundColor(mContext.getResources().getColor(R.color.ColorPrimary));
            chart.setGridBackgroundColor(Color.TRANSPARENT);
            chart.setMaxVisibleValueCount(2);

            LineData data = new LineData(xVals, set1);
            chart.setData(data);
            chart.invalidate();
        }

        private void setTransactionsChart(List<CardObj.Transaction> transactions){
            LineChart chart = chart_transactions;
            ArrayList<Entry> valsComp1 = new ArrayList<>();
            ArrayList<String> xVals = new ArrayList<>();
            int i=0;
            for(CardObj.Transaction transaction : transactions){
                Entry c1e1 = new Entry((float) transaction.cost, i++);
                valsComp1.add(c1e1);
                xVals.add(transaction.getDaysAgo()+"");
            }

            LineDataSet set1 = new LineDataSet(valsComp1, "");
            set1.setDrawCubic(true);
            set1.setCubicIntensity(0.2f);
            set1.setColor(Color.WHITE);
            set1.setCircleColor(Color.WHITE);
            set1.setLineWidth(3f);
            set1.setCircleSize(2f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(9f);
            set1.setFillAlpha(65);
            set1.setFillColor(mContext.getResources().getColor(R.color.ColorPrimary));

            chart.getLegend().setEnabled(false);
            chart.getAxisRight().setEnabled(false);
            YAxis yAxis = chart.getAxisLeft();
            yAxis.setLabelCount(3, false);
            yAxis.setTextColor(Color.WHITE);
            yAxis.setGridColor(mContext.getResources().getColor(R.color.ColorPrimaryLight));
            yAxis.setAxisLineColor(mContext.getResources().getColor(R.color.ColorPrimaryLight));

            XAxis xAxis = chart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setTextSize(10f);
            xAxis.setTextColor(Color.WHITE);
            xAxis.setDrawAxisLine(true);
            yAxis.setLabelCount(4, false);
            xAxis.setDrawGridLines(false);
            xAxis.setAxisLineColor(mContext.getResources().getColor(R.color.ColorPrimaryLight));
            xAxis.setValueFormatter(new MyCustomXAxisValueFormatter());

            chart.setDescription("");
            chart.setBackgroundColor(Color.TRANSPARENT);
            chart.setBorderColor(Color.TRANSPARENT);
            chart.setGridBackgroundColor(mContext.getResources().getColor(R.color.ColorPrimary));
            chart.setGridBackgroundColor(Color.TRANSPARENT);
            chart.setMaxVisibleValueCount(2);

            LineData data = new LineData(xVals, set1);
            chart.setData(data);
            chart.invalidate();
        }

        public class MyCustomXAxisValueFormatter implements XAxisValueFormatter {
            @Override
            public String getXValue(String original, int index, ViewPortHandler viewPortHandler) {
                int days = Integer.parseInt(original);
                Calendar calendar = Calendar.getInstance(); // this would default to now
                calendar.add(Calendar.DAY_OF_MONTH, -days);
                SimpleDateFormat format = new SimpleDateFormat("MMM d");
                return format.format(calendar.getTime());
            }
        }
    }
}
