package com.maurice.grimreaper.grimreaper.Activities.Curate;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.maurice.grimreaper.grimreaper.MainApplication;
import com.maurice.grimreaper.grimreaper.Models.SMSMessage;
import com.maurice.grimreaper.grimreaper.R;

import java.util.ArrayList;
import java.util.List;

public class SP1_SelectMessage extends AppCompatActivity {


    ArrayList<SMSMessage> allSMSMessages = new ArrayList<>();
    ListView notificationsLV;
    SwipeRefreshLayout refresh_cont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_notifications);

        notificationsLV = (ListView) findViewById(R.id.notificationsLV);

        refresh_cont = (SwipeRefreshLayout) findViewById(R.id.refresh_cont);
        refresh_cont.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setAdapterFromLocalNotifications();
            }
        });

        setAdapterFromLocalNotifications();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sp1__select_message, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public List<SMSMessage> setAdapterFromLocalNotifications() {

        allSMSMessages.clear();
        allSMSMessages.addAll(MainApplication.getInstance().smsDumpHandler.smsRawDump);
        if( notificationsLV.getAdapter() == null ){
            notificationsLV.setAdapter(new SP1_SelectMessageAdapter(this, allSMSMessages));
        }

        refresh_cont.setRefreshing(false);
        ((BaseAdapter) notificationsLV.getAdapter()).notifyDataSetChanged();

        return allSMSMessages;
    }
}
