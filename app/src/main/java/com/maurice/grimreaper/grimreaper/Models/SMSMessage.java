package com.maurice.grimreaper.grimreaper.Models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SMSMessage implements Serializable {

    public String id;
    public String number;
    public String date;
    public String body;
    public String type;

    public SMSMessage() {
    }

    //SERVER ENCODERS
    public static SMSMessage decode(JSONObject obj){

        SMSMessage SMSMessage = new SMSMessage();
        try {
            SMSMessage.id = (obj.has("id")) ? obj.getString("id") : null;
            SMSMessage.number = (obj.has("number")) ? obj.getString("number") : null;
            SMSMessage.date = (obj.has("date")) ? obj.getString("date") : null;
            SMSMessage.body = (obj.has("body")) ? obj.getString("body") : null;
            SMSMessage.type = (obj.has("type")) ? obj.getString("type") : null;
        } catch (JSONException e) {e.printStackTrace();}
        return SMSMessage;
    }

    public static ArrayList<SMSMessage> decode(JSONArray obj){

        ArrayList<SMSMessage> list = new ArrayList<>();
        for(int i=0;i<obj.length();i++){
            try {
                list.add(decode(obj.getJSONObject(i)));
            } catch (JSONException e) {e.printStackTrace();}
        }
        return list;
    }

    public JSONObject encode(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put( "id", id );
            jsonObject.put( "number", number );
            jsonObject.put( "date", date );
            jsonObject.put( "body", body );
            jsonObject.put( "type", type );

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public ArrayList<String> returnBodyAsArrStr(){
        String[] splited = body.split("\\s+");
        return new ArrayList<>(Arrays.asList(splited));
    }
    public ArrayList<String> returnBodyAsArrInt(){
        ArrayList<String> result = new ArrayList<>();
        ArrayList<String> words = returnBodyAsArrStr();
        for(String word : words){
            Pattern p = Pattern.compile("((?:\\d+\\.)?\\d+(?:\\d*\\,)?\\d*)");//TODO : double comma not working for this
            Matcher m = p.matcher(word);
            if(m.find()){
                for(int i=0;i<m.groupCount();i++){
                    result.add(m.group(i));
                }
            }
        }


        return result;
    }

    public long getTime(){
        return Long.parseLong(date);
    }
    public String getTimeString(){
        SimpleDateFormat format = new SimpleDateFormat("MMM d h:mm a");
        return format.format(new Date(getTime()));
    }



}
