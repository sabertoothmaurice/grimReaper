package com.maurice.grimreaper.grimreaper.Activities.Main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.maurice.grimreaper.grimreaper.Controllers.LocalBroadcastHandler;
import com.maurice.grimreaper.grimreaper.MainApplication;
import com.maurice.grimreaper.grimreaper.Models.BucketObj;
import com.maurice.grimreaper.grimreaper.R;
import com.maurice.grimreaper.grimreaper.Utils.Logg;
import com.maurice.grimreaper.grimreaper.Utils.Router;
import com.maurice.grimreaper.grimreaper.storage.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * This is the main fragment user for listing user Notifications
 */
public class BucketsFrag extends android.support.v4.app.Fragment {
    static final String TAG = "TransactionsFrag";
    public Context mContext;
    ArrayList<BucketObj> allRegexDefs = new ArrayList<>();
    ListView notificationsLV;
    SharedPrefs sharedPrefs = SharedPrefs.getInstance(getActivity());
    SwipeRefreshLayout refresh_cont;
    TextView tv_topline;

    private BroadcastReceiver notificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("notificationReceiver: ", " : FORUM_NOTIFICATION_RECEIVED");
            // TODO // Show a fading background for the notification that just came in real time via this broadcast
            fetchData();
        }
    };

    public BucketsFrag() {
    }

    public static BucketsFrag newInstance(Context activityContext) {
        BucketsFrag myFragment = new BucketsFrag();
        myFragment.mContext = activityContext;
        return myFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    private void fetchData(){
        String url = Router.Data.regexs();
        MainApplication.getInstance().addRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Logg.d(TAG, "USER DATA : " + jsonObject.toString());
                try {
                    JSONObject result = jsonObject.getJSONObject("result");
                    JSONArray buckets = result.has("buckets") ? result.getJSONArray("buckets") : new JSONArray();

                    allRegexDefs.clear();
                    allRegexDefs.addAll(BucketObj.decode(buckets));
                    if (notificationsLV.getAdapter() == null) {
                        notificationsLV.setAdapter(new BucketsFragAdapter(getActivity(), allRegexDefs));
                    }
                    tv_topline.setText(allRegexDefs.size()+ " buckets found");

                    refresh_cont.setRefreshing(false);
                    ((BaseAdapter) notificationsLV.getAdapter()).notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Logg.e(TAG, "ERROR : " + volleyError);
                refresh_cont.setRefreshing(false);
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();

        // Register mMessageReceiver to receive messages.
        LocalBroadcastManager.getInstance(mContext).registerReceiver(notificationReceiver, new IntentFilter(LocalBroadcastHandler.MESSAGES_UPLOADED));

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView;
        rootView = inflater.inflate(R.layout.fragment_buckets, null);

        notificationsLV = (ListView) rootView.findViewById(R.id.notificationsLV);
        tv_topline = (TextView) rootView.findViewById(R.id.tv_topline);

        refresh_cont = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh_cont);
        refresh_cont.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchData();
            }
        });
        fetchData();
        return rootView;
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        for(int i=0;i<menu.size();i++){
            menu.getItem(i).setVisible(false);
        }
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver( notificationReceiver );
        super.onPause();
    }
}
