package com.maurice.grimreaper.grimreaper.Models;

import android.text.Spanned;

import com.maurice.grimreaper.grimreaper.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class BucketObj implements Comparable<BucketObj> {

    public List<String> samples = new ArrayList<>();
    public List<String> variables = new ArrayList<>();
    public String vendorId;
    public Long time;
    public String _id,category,regex;
    public String raw,from,name;
    public HashMap<String,Object> extras = new HashMap<>();
    public int matches;


    public BucketObj() {}

    //SERVER ENCODERS
    public static BucketObj decode(JSONObject obj){

        BucketObj re = new BucketObj();
        try {
            JSONArray sampleArr = (obj.has("samples")) ? obj.getJSONArray("samples") : new JSONArray();
            for(int i=0;i<sampleArr.length();i++) re.samples.add(sampleArr.getString(i));
            JSONArray variablesArr = (obj.has("variables")) ? obj.getJSONArray("variables") : new JSONArray();
            for(int i=0;i<variablesArr.length();i++) re.variables.add(variablesArr.getString(i));
            re.vendorId = (obj.has("vendorId")) ? obj.getString("vendorId") : null;
            re.time = (obj.has("time")) ? obj.getLong("time") : Calendar.getInstance().getTimeInMillis();
            re._id = (obj.has("_id")) ? obj.getString("_id") : null;
            re.from = (obj.has("from")) ? obj.getString("from") : null;
            re.name = (obj.has("name")) ? obj.getString("name") : null;
            re.category = (obj.has("category")) ? obj.getString("category") : null;
            re.matches = (obj.has("matches")) ? obj.getInt("matches") : 0;

            re.regex = (obj.has("regex")) ? obj.getString("regex") : null;

            JSONObject extrasJSON = (obj.has("extras")&&obj.get("extras") instanceof JSONObject) ? obj.getJSONObject("extras") : new JSONObject();
            Iterator<?> keys = extrasJSON.keys();
            while( keys.hasNext()) {
                String key = (String) keys.next();
                re.extras.put(key,extrasJSON.get(key));
            }

            re.raw = obj.toString(4);
        } catch (JSONException e) {e.printStackTrace();}
        return re;
    }

    public static ArrayList<BucketObj> decode(JSONArray obj){

        ArrayList<BucketObj> list = new ArrayList<>();
        for(int i=0;i<obj.length();i++){
            try {
                list.add(decode(obj.getJSONObject(i)));
            } catch (JSONException e) {e.printStackTrace();}
        }
        return list;
    }

    public JSONObject encode(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put( "samples", samples);
            jsonObject.put( "vendorId", vendorId );
            jsonObject.put( "time", time );
            jsonObject.put( "_id", _id );
            jsonObject.put( "raw", raw );

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public int compareTo(BucketObj another) {
        return 0;//TODO : implement this
    }

    public Spanned getHighlightedBody(){
        String body = ""+regex;
        body = body.replace("\\","");
        ArrayList<String> highlightTextArr = new ArrayList<>();
        for(String varName : variables){
            int index = body.indexOf("(.+)");
            body = body.substring(0, index) + "["+varName+"]" + body.substring(index+4);
            highlightTextArr.add("["+varName+"]");
        }
        return Utils.highlightText(highlightTextArr, body);
    }
}
