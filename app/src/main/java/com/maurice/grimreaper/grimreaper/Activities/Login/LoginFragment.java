package com.maurice.grimreaper.grimreaper.Activities.Login;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.maurice.grimreaper.grimreaper.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {
    View mainView;
    int viewId = R.layout.fragment_login_page1;

    public static LoginFragment newInstance(int viewId) {
        LoginFragment fragment = new LoginFragment();
        fragment.viewId = viewId;
        return fragment;
    }

    public LoginFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        mainView = inflater.inflate(viewId, container, false);
        return mainView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
